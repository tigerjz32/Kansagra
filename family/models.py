from django.db import models

class familyInfo(models.Model):
    userId = models.CharField(max_length=200,null=True, blank=True)
    name = models.CharField(max_length=200,null=True, blank=True)
    image = models.CharField(max_length=200,null=True, blank=True)
    
    def __unicode__(self):
        return unicode(self.name) or u''
